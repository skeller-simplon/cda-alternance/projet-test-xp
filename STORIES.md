En tant que ... je veux pouvoir ... [afin de ...]

### En tant qu'utilisateur je veux pouvoir consulter des évènements. ###
    Je veux pouvoir les filtrer par adresse.
    Je veux pouvoir les filtrer par catégorie.

### En tant qu'utilisateur je veux pouvoir ajouter des évènements. ###
    L'évènement doit être composé
        - d'une date de début.
        - un titre.
        - d'un tag/catégorie.
    L'évènement peut être composé de
        - une date de fin.
        - une description.
        - une adresse.
            - unique pour une même date
    Dans une version prochaine, l'évènement
        - sera composé d'un utilisateur créateur.

### En tant qu'utilisateur je veux pouvoir modifier des évènements. ###
    Dans une version prochaine, l'évènement
        - ne sera modifiable que par son créateur.

### En tant qu'utilisateur je veux pouvoir supprimer des évènements. ###
    Dans une version prochaine, l'évènement
        - ne sera modifiable que par son créateur.

### En tant qu'utilisateur je veux pouvoir partager le lien unique d'un évènement. ###
