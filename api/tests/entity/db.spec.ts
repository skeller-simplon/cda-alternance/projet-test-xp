import { Event } from "../../src/entity/Event"
import { EventCategory } from "../../src/entity/EventCategory"
import setupConnection from "../../src/connectionSetup";
import {connectionOptionsTest} from "../../src/utils/connection-options";
import { getConnection } from "typeorm";

describe('classe db', () => {

    beforeEach(async () => {
        await setupConnection(connectionOptionsTest)
    });

    afterEach(async () => {
        await getConnection().close();
    });


    it("doit vérifier que la table Event est accessible", async done => {
        let event = new Event();
        let co = getConnection();

        co.manager.save(event).then(async bddEvent => {
            let found = await co.getRepository(Event).find(bddEvent)
            expect(found).toBeTruthy();
            done();
        })
    })

    // it("doit vérifier que l'Event corresponde à ses cas limites", async done => {
    //     let event = new Event();
    //     let nowTime = new Date();
    //     let endtime = new Date();
    //     endtime.setHours(endtime.getHours() + 1)
    //     endtime.setMilliseconds(0)
    //     event.startDate = nowTime;
    //     event.endDate = endtime;
    //     event.title = "TITLE";
    //     event.description = "DESCRIPTION";
    //     event.address = "ADDRESS";
    //     let co = await global.connection.get();
    //     event.category = await co.getRepository(EventCategory).findOne() as EventCategory;
    //     co.getRepository(Event).save(event).then(async bddEvent => {
    //         expect(bddEvent.id).toBeTruthy()
    //         expect(bddEvent.startDate).toBeTruthy()
    //         expect(bddEvent.endDate).toBeTruthy()
    //         expect(bddEvent.title).toStrictEqual("TITLE")
    //         expect(bddEvent.description).toStrictEqual("DESCRIPTION")
    //         expect(bddEvent.address).toStrictEqual("ADDRESS")
    //         done();
    //     })
    // })
})
