import request from 'supertest';
import app from '../../src/app';

import setupConnection from "../../src/connectionSetup";
import { connectionOptionsTest } from "../../src/utils/connection-options";
import { getConnection, Connection } from "typeorm";
import { Event } from "../../src/entity/Event";

describe('events', () => {

    let connection: Connection;
    beforeEach(async () => {
        await setupConnection(connectionOptionsTest)
        connection = await getConnection();

    });

    afterEach(async () => {
        await getConnection().close();
    });

    it('test nul', async done => {
        connection.getRepository(Event).query("SELECT 1;").then(r => {
            expect(r).toBeTruthy()
            done()
        })
    })

    it('/get categories', async () => {
        const res = await request(app)
        expect(res).toBeTruthy()
        let supertestRequest = await res.get('/event/categories/all')
        expect(supertestRequest.status).toBe(200);
    });

    it('should add an event in database', async () => {

        await request(app)
            .post('/event')
            .send({
                title: "test title",
                description: "description",
                address: "teste addresse",
                startDate: new Date().toISOString(),
                endDate: new Date().toISOString(),
            })
            .expect(201)
            .then(res => {
                expect(res.body).toStrictEqual({
                    success: true,
                    userPosted: 1
                })
            });
    });

    it('/get after /create', async done => {
        let date1: string | Date = new Date();
        let date2: string | Date = new Date();

        date1.setMilliseconds(0);
        date2.setMilliseconds(0);

        date1 = date1.toISOString();
        date2 = date2.toISOString();
        
        let eventCreated = await request(app)
            .post('/event')
            .set('Accept', "json")
            .set('Content-Type', 'application/json')
            .send({
                title: "test title",
                description: "description",
                address: "teste addresse",
                startDate: date1,
                endDate: date2,
            })

        expect(eventCreated.status).toStrictEqual(201);
        const res = await request(app)
            .get('/event?from=2000-01-01&to=2022-12-12')
            ;

        expect(res.status).toBe(200);
        expect(res.body).toBeInstanceOf(Array);

        expect(res.body[0]).toStrictEqual({
            id: 1,
            title: "test title",
            description: "description",
            address: "teste addresse",
            startDate: date1.toString(),
            endDate: date2.toString(),
        });
        done()
    });

    it('should delete an event in database', async () => {

        let eventCreated = await request(app)
            .post('/event')
            .set('Accept', "json")
            .set('Content-Type', 'application/json')
            .send({
                title: "test title",
                description: "description",
                address: "teste addresse",
                startDate: new Date().toISOString(),
                endDate: new Date().toISOString(),
            })
        const [eventInitial, firstEventCount] = await connection.getRepository(Event).findAndCount();

        const res = await request(app).delete('/event/' + eventInitial[0].id);
        expect(res.status).toBe(204);
        const [events, lastEventCount] = await connection.getRepository(Event).findAndCount();

        expect(events.length).toBe(0);
        expect(firstEventCount - lastEventCount).toBe(1);
    });

    it('should update an event in database', async () => {
        let eventCreated = await request(app)
        .post('/event')
        .set('Accept', "json")
        .set('Content-Type', 'application/json')
        .send({
            title: "test title",
            description: "description",
            address: "teste addresse",
            startDate: new Date().toISOString(),
            endDate: new Date().toISOString(),
        })
        
        const res = await request(app)
            .put('/event/1')
            .send({
                title: "test title after update",
                description: "description after update",
                address: "teste addresse after update",
                startDate: new Date().toISOString(),
                endDate: new Date().toISOString(),
            });

        expect(res.status).toBe(200);
        expect(res.body.title).toEqual("test title after update");

    });
});