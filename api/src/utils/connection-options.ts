import dotenv from "dotenv";
dotenv.config()

import { ConnectionOptions } from "typeorm";
import { Event } from "../entity/Event"
import { EventCategory } from "../entity/EventCategory"

const ENTITIES = [Event, EventCategory];
const HOST = "localhost";

const connectionOptions: ConnectionOptions = {
    type: "mysql",
    database: process.env.SQl_DATABASE_NAME,
    username: process.env.SQL_USERNAME,
    password: process.env.SQL_PASSWORD,
    host: HOST,
    entities: ENTITIES,
    synchronize: true,
    dropSchema: true
}

const connectionOptionsTest: ConnectionOptions = {
    type: 'mysql',
    database: process.env.SQl_DATABASE_NAME,
    username: process.env.SQL_USERNAME,
    password: process.env.SQL_PASSWORD,
    host: HOST,
    entities: ENTITIES,
    synchronize: true,
    dropSchema: true
}
export { connectionOptions, connectionOptionsTest };