import { Request as Req, Response as Res } from "express";
import { Event } from "../entity/Event";

import { EventCategory } from "../entity/EventCategory";
import { getConnection, LessThan, MoreThan, Repository, getRepository } from "typeorm";

type EventDTO = {
    title: string,
    description: string,
    address: string,
    tag: string,
    startDate: Date,
    endDate: Date
}

export default class CalendarController {

    private fetchRepository(): Repository<Event> {
        return getRepository(Event)
    }

    async addEvent(req: Req, res: Res) {
        console.log(req.body);
        req.body.startDate = new Date(req.body.startDate);
        req.body.endDate = new Date(req.body.endDate);
        try {
            const repo = this.fetchRepository();
            const event = await repo.save({
                ...req.body
            })

            res.status(201).send({
                success: true,
                userPosted: 1
            });
        } catch (error) {
            res.status(400).send(error);
        }
    }

    async updateEvent(req: Req, res: Res) {
        try {
            const repo = this.fetchRepository();
            const event = await repo.save({
                ...req.body
            });

            res.status(200).send(event)
        } catch (error) {
            res.status(400).send(error)
        }
    }


    async getEvent(req: Req, res: Res) {
        try {
            const repo = this.fetchRepository();
            const result = await repo.findOne({ id: parseInt(req.params.id) });
            res.status(200).send(result)
        } catch (error) {
            res.status(400).send(error)
        }
    }

    async getEvents(req: Req, res: Res) {
        // TODO : Vérifier que query.from & query.to est parsable comme string.
        try {
            const repo = this.fetchRepository();

            const result = await repo.find({
                startDate: MoreThan(new Date(req.query.from as string)),
                endDate: LessThan(new Date(req.query.to as string))
            });
            res.status(200).send(result)
        } catch (error) {
            res.status(400).send(error)
        }

    }

    async getEventsCategories(req: Req, res: Res) {
        try {
            const repo = getRepository(EventCategory)
            let categories = await repo.find();

            res.status(200).send(categories);
        } catch (error) {
            res.status(400).send(error)
        }
    }

    async deleteEvent(req: Req, res: Res) {
        try {
            const repo = getRepository(Event);
            let retValue = await repo.delete(parseInt(req.params.id));
            res.status(204).send(retValue);
        } catch (error) {
            res.status(400).send(error)
        }
    }
}