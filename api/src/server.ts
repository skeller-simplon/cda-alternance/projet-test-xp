import app from './app';
import setupConnection from "./connectionSetup"
import {connectionOptions} from "./utils/connection-options"

setupConnection(connectionOptions).then(() => {
    app.listen(3000, () => {
        console.log('server listening on port 3000')
    });
})