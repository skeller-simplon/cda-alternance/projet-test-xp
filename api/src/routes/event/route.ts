import bodyParser from "body-parser";
import { Router } from "express";
import CalendarController from "../../controller/calendar";

const controller = new CalendarController();

const router = Router();

router.post('/', controller.addEvent.bind(controller));

router.delete('/:id', controller.deleteEvent.bind(controller));

router.put('/:id', controller.updateEvent.bind(controller))

router.get('/:id', controller.getEvent.bind(controller));

router.get('/', controller.getEvents.bind(controller))

router.get('/categories/all', controller.getEventsCategories.bind(controller))


export default router;
