import eventRoutes from './route';
import { Router } from 'express';

const router = Router();

router.use('/event', eventRoutes);

export default router;