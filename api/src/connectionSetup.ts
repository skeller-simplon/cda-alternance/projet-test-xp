import "reflect-metadata";
import { ConnectionOptions, createConnection, getConnection } from 'typeorm';
import dotenv from "dotenv";
import { Event } from './entity/Event';
import { EventCategory } from "./entity/EventCategory";

dotenv.config()


async function setupConnection(opts: ConnectionOptions){
    await createConnection(opts);
    return getConnection().synchronize();

}
export default setupConnection;