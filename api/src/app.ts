import express from 'express';
import eventRoutes from './routes/event';
import bodyParser from 'body-parser';
import cors from "cors";

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(eventRoutes);
// app.get('/event', (req, res) => {
//     console.log('coucou');

//     res.send({success: true});
// });

export default app;