import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from "typeorm";
import { EventCategory } from "./EventCategory"

@Entity()
export class Event {

    @Column()
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ default: "" })
    title: string;

    @Column({ nullable: true })
    description: string;

    @Column({ nullable: true })
    address: string;

    @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP" })
    startDate: Date;

    @Column({ nullable: true })
    endDate: Date;

    @ManyToOne(() => EventCategory, eventCategory => eventCategory.events)
    category:EventCategory;

}