import { Entity, Column, PrimaryGeneratedColumn, OneToMany} from "typeorm";
import {Event} from "./Event";

@Entity()
export class EventCategory {

  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @OneToMany(() => Event, event => event.category)
  public events: Event[];

}